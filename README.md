NoTrack 0.9 is still in development, but can be tested out.  
Stable version of NoTrack 0.8.x can be found at [GitHub](https://github.com/quidsup/notrack)  
  
To Install:  
```bash
wget https://gitlab.com/quidsup/notrack/raw/master/install.sh
bash install.sh
```